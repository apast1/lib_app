package com.library.library_app.service;

import com.library.library_app.converter.BookConverter;
import com.library.library_app.converter.BookDtoConverter;
import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Author;
import com.library.library_app.entity.Book;
import com.library.library_app.entity.Publisher;
import com.library.library_app.repository.BookRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.AssertTrue;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Boolean.FALSE;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BookServiceImplTest {

    @Autowired
    private BookService bookService;
    @Autowired
    private BookRepository bookRepository;

    @Test
    public void shouldFindBookById() throws Exception {
        // Author author=new Author();
        // Publisher publisher=new Publisher();

        //  BookDto bookDto = new BookDto(1L,"Home",author,publisher,"DRAMA",FALSE);

        Optional<Book> byId = bookRepository.findById(1L);
        assertTrue(byId.isPresent());
        assertEquals("HumanWorld", byId.get().getTitle());

    }

    @Test
    public void shuldFindAllBooks() {
        Iterable<Book> all = bookRepository.findAll();
        List<Book> bookList = new ArrayList();

        for (Book book : all) {
            bookList.add(book);

        }
        assertTrue(bookList.size() == 4);

    }

    @Test
    public void shouldAddBookDto() {
        Author author = new Author();
        Publisher publisher = new Publisher();
        BookDto bookDtoAdded = new BookDto(5L, "Jez", author, publisher, "DRAMA", Boolean.FALSE);
        bookService.addBookDto(bookDtoAdded);
        Optional<Book> byId = bookRepository.findById(5L);
        assertNotNull(byId);
        assertEquals("Jez", byId.get().getTitle());
    }

    @Test
    public void shouldFindAllTitles() {
        List<String> allTitlesOfBooks = bookService.findAllTitles();
        assertEquals(4, allTitlesOfBooks.size());
        String firstTitle = allTitlesOfBooks.get(0);
        assertEquals("HumanWorld", firstTitle);
    }

    @Test
    public void shouldFindBooksNotRented() {
        List<BookDto> booksNotRented = bookService.findBooksNotRented();
        assertEquals(1, booksNotRented.size());


    }

}




