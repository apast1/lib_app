package com.library.library_app.service;

import com.library.library_app.entity.LibraryClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class LibraryClientServiceImplTest {

    @Autowired
    private LibraryClientService libraryClientService;



    @Test
    public void shouldFindAllClients() {
        int howManyClients = libraryClientService.findAll().size();
        assertTrue(howManyClients==3);
        List<LibraryClient> allClients = libraryClientService.findAll();
        assertEquals("jan",allClients.get(0).getFirstName());
        assertEquals("niedzielniak",allClients.get(0).getLastName());
    }

    @Test
    public void findByCardId() {
    }
}