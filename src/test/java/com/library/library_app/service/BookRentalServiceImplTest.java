package com.library.library_app.service;

import com.library.library_app.entity.Book;
import com.library.library_app.entity.BookRental;
import com.library.library_app.entity.LibraryClient;
import com.library.library_app.repository.BookRentalRepository;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BookRentalServiceImplTest {
    @Autowired
    private BookRentalService bookRentalService;
    @Autowired
    private BookRentalRepository bookRentalRepository;

    private Book book;
    private BookRental bookRental;
    private LibraryClient libraryClient;


    @Before
    public void init() {
        book = new Book();
        book.setBorrowed(Boolean.FALSE);
        book.setTitle("Hollywood");
        libraryClient = new LibraryClient();
        libraryClient.setFirstName("Anna");
        libraryClient.setLastName("Bajeczna");
        bookRental = new BookRental();
        bookRental.setBook(book);
        bookRental.setLibraryClient(libraryClient);
        bookRental.setActualReturnDate(LocalDate.of(10, 10, 15));

    }


    @Test
    public void addBookRental() {
        assertEquals(Boolean.FALSE, book.getBorrowed());
        BookRental addedBookRental = bookRentalService.addBookRental(bookRental);
        assertEquals(bookRental.getActualReturnDate(), addedBookRental.getActualReturnDate());
        assertEquals("Hollywood", addedBookRental.getBook().getTitle());
        assertEquals(Boolean.TRUE, addedBookRental.getBook().getBorrowed());

    }

    @Test
    public void shouldReturnBook() {
        bookRentalService.addBookRental(bookRental);
        assertEquals(Boolean.TRUE, bookRental.getBook().getBorrowed());
        bookRentalService.returnBook(bookRental);
        assertEquals(Boolean.FALSE, bookRental.getBook().getBorrowed());
        assertNotEquals(Boolean.TRUE, bookRental.getBook().getBorrowed());


    }

    @Test
    public void shouldFindById() throws Exception {

        BookRental addedBookRental = bookRentalService.addBookRental(bookRental);

        assertEquals("Hollywood", bookRentalService.findById(addedBookRental.getId()).getBook().getTitle());
    }

    @Test
    public void findBooksRentedAndNotReturnedYetByClient() {
        int bookListSize = bookRentalService.findBooksRentedByClient(1L).size();
        String title = bookRentalService.findBooksRentedByClient(1L).get(0);

        assertTrue(bookListSize == 1);
        assertEquals("Kill him", title);

    }

    @Test
    public void shouldFindBookRentalIdWhenGivenClientIdAndBookTitle() {
        Long bookRentalFoundId = bookRentalService.findBookRentalByClientIdAndBookTitle(1L, "HumanWorld");
        assertEquals(Long.valueOf(1),bookRentalFoundId);

    }
}