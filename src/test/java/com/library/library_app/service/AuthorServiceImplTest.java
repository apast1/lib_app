package com.library.library_app.service;

import com.library.library_app.entity.Author;
import com.library.library_app.repository.AuthorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorServiceImplTest {
    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorRepository authorRepository;

    private Author author;

    @Before
    public void init() {
    }

    @Test
    public void findAuthor() {
        Author foundAuthor = authorService.findAuthor("jan", "nowak");
        assertNotNull(foundAuthor);
        //now check - if not found creates new one
        Author notExistingAuthor = authorService.findAuthor("Mumbaha", "Huraha");
        assertNotNull(notExistingAuthor);
    }
}