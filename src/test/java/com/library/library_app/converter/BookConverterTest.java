package com.library.library_app.converter;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Author;
import com.library.library_app.entity.Book;

import com.library.library_app.entity.BookType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookConverterTest {

    @Autowired
    private BookConverter bookConverter;

    @Before


    @Test
    public void shouldConvertBookDtoToBook() {

        Author author = new Author();
        author.setLastName("marecki");
        author.setFirstName("janek");
        author.setId(1l);
        BookDto book = new BookDto();
        book.setId(1l);
        book.setTitle("Humorzasty pan");
        book.setBorrowed(false);
        book.setAuthor(author);

        book.setBookType("ACTION");
        BookDto book1 = new BookDto();
        book1.setId(2l);
        book1.setTitle("Humorzasta pani0");
        book1.setBorrowed(false);
        book1.setAuthor(author);
        book1.setBookType("DRAMA");

        BookConverter bookConverter = new BookConverter();
        Book convertedBookDto = bookConverter.apply(book);
        Assert.assertEquals(convertedBookDto.getAuthor(), book.getAuthor());
        Assert.assertEquals(convertedBookDto.getBookType().toString(), book.getBookType());
        Assert.assertEquals(convertedBookDto.getId(),book.getId());
        Assert.assertEquals(convertedBookDto.getPublisher(),book.getPublisher());
        Assert.assertEquals(convertedBookDto.getTitle(),"Humorzasty pan");


    }


}
