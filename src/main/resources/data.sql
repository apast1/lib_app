INSERT INTO client_address( city, street, zipcode,block_number,flat_number)
 VALUES ('warsaw', 'polna', '02-972',1,2);
INSERT INTO client_address(city, street, zipcode,block_number,flat_number)
 VALUES ('poznan', 'pulaskiego', '02-600',2,40);
INSERT INTO client_address(city, street, zipcode,block_number,flat_number)
 VALUES ('krakow', 'polska', '04-400',1,100);
INSERT INTO author(first_name,last_name)
 VALUES ('jan','nowak');
INSERT INTO author(first_name,last_name)
 VALUES ('kalina','polak');
INSERT INTO author(first_name,last_name)
 VALUES ('andrzej','babal');
INSERT INTO book(book_type, is_borrowed, pages, title, author_id, publisher_id)
 VALUES ('SCIENCE_FICTION',TRUE ,100,'HumanWorld',1,1);
INSERT INTO book(book_type, is_borrowed, pages, title, author_id,  publisher_id)
 VALUES ('DRAMA',TRUE ,256,'Kill him',2,2);
INSERT INTO book(book_type, is_borrowed, pages, title, author_id,  publisher_id)
 VALUES ('ROMANCE',FALSE ,50,'I want to marry you',3,3);
INSERT INTO book(book_type, is_borrowed, pages, title, author_id, publisher_id)
VALUES ('SCIENCE_FICTION',FALSE ,100,'New Book',1,1);
INSERT INTO book_rental(actual_return_date, date_to_be_returned, rental_date,book_id, library_client_id)
 VALUES (NULL,'18.09.29','18.09.01',1,1);
INSERT INTO book_rental(actual_return_date, date_to_be_returned, rental_date,book_id, library_client_id)
 VALUES (NULL,'18.07.29','18.07.01',2,1);
INSERT INTO book_rental(actual_return_date, date_to_be_returned, rental_date,book_id, library_client_id)
 VALUES ('18.06.29','18.06.29','18.06.01',3,2);
INSERT INTO library_client(email_address,first_name,last_name,telephone_no,address_id)
 VALUES ('jan@wp.pl','jan','niedzielniak','648600902',1);
INSERT INTO library_client(email_address,first_name,last_name,telephone_no,address_id)
 VALUES ('anna@wp.pl','anna','niedzielniak','648600903',1);
INSERT INTO library_client(email_address,first_name,last_name,telephone_no,address_id)
 VALUES ('monika@wp.pl','monika','podolska','228443961',2);
INSERT INTO publisher(name)
 VALUES ('iskry');
INSERT INTO publisher(name)
 VALUES ('wiedza o zyciu');
INSERT INTO publisher(name)
 VALUES ('nasza ksiegarnia');


