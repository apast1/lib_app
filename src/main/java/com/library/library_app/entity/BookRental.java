package com.library.library_app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Entity
public class BookRental {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(name = "library_client_id")
    @JsonBackReference(value = "rental-client")
    @NotNull(message="Please choose client card ID")
    @Valid
    private LibraryClient libraryClient;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    @JsonBackReference(value = "book-rental")
    @NotNull(message="Please choose book to be returned")
    @Valid
    private Book book;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @PastOrPresent(message="Please choose rental date from calendar!")
    @Valid
    private LocalDate rentalDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateToBeReturned;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate actualReturnDate;

    public BookRental() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LibraryClient getLibraryClient() {
        return libraryClient;
    }

    public void setLibraryClient(LibraryClient libraryClient) {
        this.libraryClient = libraryClient;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public LocalDate getRentalDate() {
        return rentalDate;
    }
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public void setRentalDate(LocalDate rentalDate) {
        this.rentalDate = rentalDate;
    }
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    public LocalDate getDateToBeReturned() {
        return dateToBeReturned;
    }

    public void setDateToBeReturned(LocalDate rentalDate) {

        this.dateToBeReturned =rentalDate.plusDays(30);
    }



    public LocalDate getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(LocalDate actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }
}
