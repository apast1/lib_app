package com.library.library_app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class LibraryClient {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long cardId;
    private String firstName;
    private String lastName;
    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name="address_id")
    @JsonBackReference
    private ClientAddress address;

    private String telephoneNo;
    private String emailAddress;

    @OneToMany(mappedBy="libraryClient")
    @JsonManagedReference (value="rental-client")
    private Set<BookRental> bookRental=new HashSet<BookRental>();

    public LibraryClient() {
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ClientAddress getAddress() {
        return address;
    }

    public void setAddress(ClientAddress address) {
        this.address = address;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Set<BookRental> getBookRental() {
        return bookRental;
    }

    public void setBookRental(Set<BookRental> bookRental) {
        this.bookRental = bookRental;
    }
}
