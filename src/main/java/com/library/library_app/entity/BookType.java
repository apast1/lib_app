package com.library.library_app.entity;

import javax.persistence.Entity;


public enum BookType {
    SCIENCE_FICTION,
    DRAMA,
    ACTION,
    ROMANCE,
    GUIDE,
    HORROR,
    CHILDRENS,
    BIOGRAPHIES,
    HISTORY,
    PROGRAMMING;

}
