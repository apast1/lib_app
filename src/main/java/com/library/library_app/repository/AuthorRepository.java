package com.library.library_app.repository;

import com.library.library_app.entity.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository extends CrudRepository<Author, Long> {


    Optional<Author>findByFirstNameAndLastName(String firstName, String lastName);
    @Query(value = "SELECT * FROM author ", nativeQuery = true)
    public List<Author> findAuthorByFirstId();

    @Query(value = "SELECT * FROM author WHERE author.id=1 ", nativeQuery = true)
    public List<Author> findAuthorById();



}
