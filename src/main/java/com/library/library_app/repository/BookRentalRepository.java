package com.library.library_app.repository;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.BookRental;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface BookRentalRepository extends CrudRepository<BookRental, Long> {

    @Query(value = "SELECT title FROM book b, book_rental r WHERE b.id=r.book_id AND r.library_client_id=? AND r.actual_return_date is NULL", nativeQuery = true)
    public List<String> findBooksRentedByClient(Long id);

    @Query(value = "SELECT r.id FROM book_rental r, book b, library_client l WHERE b.id=r.book_id AND r.library_client_id=l.card_id AND r.actual_return_date is NULL AND l.card_id=?1 AND b.title=?2", nativeQuery = true)
    public Long findBookRentalByClientIdAndBookTitle(Long id, String title);

    @Query(value = "SELECT date_to_be_returned FROM book_rental WHERE book_id=?", nativeQuery = true)
    public LocalDate findDateOfPossibleReturn(Long id);
}
