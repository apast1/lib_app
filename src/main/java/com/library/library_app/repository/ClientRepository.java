package com.library.library_app.repository;

        import com.library.library_app.entity.LibraryClient;
        import org.springframework.data.jpa.repository.Query;
        import org.springframework.data.repository.CrudRepository;

        import java.math.BigInteger;
        import java.util.List;

public interface ClientRepository extends CrudRepository<LibraryClient,Long>{
        @Query(value="SELECT DISTINCT l.card_id, l.email_address, l.first_name, l.last_name, l.telephone_no,l.address_id\n" +
                " FROM library_client l, book_rental br,book b WHERE l.card_id=br.library_client_id AND br.actual_return_date IS NULL;", nativeQuery=true)
        public  List<LibraryClient> findClientsWithBooksToBeReturned();
}
