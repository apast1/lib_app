package com.library.library_app.exception;

public class AuthorNotFoundException extends Exception{
    public AuthorNotFoundException(String s) {
        super(s);
    }

    public AuthorNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }


}
