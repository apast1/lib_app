package com.library.library_app.exception;

public class ClientNotFoundException extends Exception {

    public ClientNotFoundException(String s) {
        super(s);
    }

    public ClientNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
