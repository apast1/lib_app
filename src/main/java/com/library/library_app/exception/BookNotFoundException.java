package com.library.library_app.exception;

public class BookNotFoundException extends Exception{
    public BookNotFoundException(String s) {
        super(s);
    }

    public BookNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
