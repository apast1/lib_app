package com.library.library_app.exception;

public class BookRentalNotFoundException extends Exception {
    public BookRentalNotFoundException(String s) {
        super(s);
    }

    public BookRentalNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
