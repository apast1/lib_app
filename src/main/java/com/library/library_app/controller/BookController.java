package com.library.library_app.controller;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Book;
import com.library.library_app.exception.BookNotFoundException;
import com.library.library_app.repository.BookRepository;
import com.library.library_app.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;




    @GetMapping(value = "/books")
    public List<BookDto> findAllBooks() {
        return bookService.findAll();
    }


    @GetMapping(value = "/books/{id}")
    public BookDto findBookById(@PathVariable Long id) {
        try {
            return bookService.findById(id);
        } catch (BookNotFoundException e) {
            return null;
        }


    }

    @PostMapping(value="/addBook")
    public BookDto addBook(@RequestBody BookDto bookDto)
    {
        bookService.addBookDto(bookDto);

        return bookDto;
    }


}
