package com.library.library_app.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MessageController {
    @GetMapping("/message")
    public String message(@RequestParam(name="msg") String msg, Model model){
       // msg="You have successfully added new Book to the Library";
        model.addAttribute("msg",msg);
        return "message";
    }

    @GetMapping("/messageRental")
    public String messageRental(@RequestParam(name="msg") String msg, Model model){
        msg="You have successfully added new BookRental to the Library";
        model.addAttribute("msg",msg);

        return "message";
    }


}
