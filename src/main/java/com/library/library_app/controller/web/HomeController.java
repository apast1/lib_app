package com.library.library_app.controller.web;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController{
@GetMapping(value="/library",produces = MediaType.TEXT_HTML_VALUE)
        public String getStarted() {
            return "homeLibrary";
        }
}
