package com.library.library_app.controller.web;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Author;
import com.library.library_app.entity.BookType;
import com.library.library_app.exception.BookNotFoundException;
import com.library.library_app.service.AuthorService;
import com.library.library_app.service.BookRentalService;
import com.library.library_app.service.BookService;
import com.library.library_app.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@Controller
public class BookWebController {
    @Autowired
    private BookService bookService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private PublisherService publisherService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private BookRentalService bookRentalService;


    @GetMapping(value = "/books/webList", produces = MediaType.TEXT_HTML_VALUE)
    public String findAllBooks(Model model) {
        List<BookDto> bookList = bookService.findAll();


        model.addAttribute("bookList", bookList);
        return "bookList";
    }

    @GetMapping(value = "/book/{id}", produces = MediaType.TEXT_HTML_VALUE)
    public String findBookById(@PathVariable Long id, Model model) {
        try {
            BookDto bookById = bookService.findById(id);
            model.addAttribute("bookWithId", bookById);
            LocalDate dateOfPossibleReturn = bookRentalService.findDateOfPossibleReturn(id);
            model.addAttribute("possibleDateOfReturn", dateOfPossibleReturn);
            return "bookWithId";
        } catch (BookNotFoundException e) {
            return null;
        }
    }

    @GetMapping(value = "/addWebBook", produces = MediaType.TEXT_HTML_VALUE)
    public String addBookForm(Model model) {

        model.addAttribute("bookTypes", BookType.values());
        model.addAttribute("publisherList", publisherService.findAll());


        BookDto bookDto = new BookDto();
        model.addAttribute("addedBook", bookDto);


        return "addBookWithBootstrap";//"addBook";
    }

    @PostMapping("/addWebBook")
    public String addBook(@Valid @ModelAttribute(value="addedBook") BookDto addedBook, BindingResult bindingResult, Model model) {
        Author author1 = addedBook.getAuthor();
        Author author = authorService.findAuthor(author1.getFirstName(), author1.getLastName());
        addedBook.setAuthor(author);
        if (bindingResult.hasErrors()) {
            model.addAttribute("bookTypes",BookType.values());
            model.addAttribute("publisherList",publisherService.findAll());
            return "addBookWithBootstrap";
        } else {
            bookService.addBookDto(addedBook);

            String bookAddedMessage = messageSource.getMessage("book.adding.success.message",
                    new Object[]{addedBook.getTitle(), addedBook.getAuthor()}, Locale.getDefault());

            return "redirect:/message?msg=" + bookAddedMessage;
        }
    }


}
