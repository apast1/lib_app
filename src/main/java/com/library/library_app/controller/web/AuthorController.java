package com.library.library_app.controller.web;

import com.library.library_app.entity.Author;
import com.library.library_app.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class AuthorController {
    @Autowired
    private AuthorService authorService;
    @GetMapping(value="/returnAuthor")
    public List<Author> findAuthorByFirstId(){
        return authorService.findAuthorByFirstId();
    }

    @GetMapping(value="/returnAuthor1")
    public @ResponseBody List<Author> getFirstAuthor(){
        return authorService.findAuthorById();
    }



}






