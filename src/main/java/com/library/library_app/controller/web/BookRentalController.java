package com.library.library_app.controller.web;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Book;
import com.library.library_app.entity.BookRental;
import com.library.library_app.entity.LibraryClient;
import com.library.library_app.exception.BookRentalNotFoundException;
import com.library.library_app.service.AuthorService;
import com.library.library_app.service.BookRentalService;
import com.library.library_app.service.BookService;
import com.library.library_app.service.LibraryClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
public class BookRentalController {

    @Autowired
    private BookService bookService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private BookRentalService bookRentalService;
    @Autowired
    private LibraryClientService libraryClientService;


    @GetMapping(value = "/addBookRentalByTitle", produces = MediaType.TEXT_HTML_VALUE)

    public String addBookRentalForm(Model model) {
        List<LibraryClient> clientList = libraryClientService.findAll();

        List<BookDto> bookList = bookService.findBooksNotRented();

        model.addAttribute("clientList", clientList);


        model.addAttribute("bookList", bookList);


        BookRental bookRental = new BookRental();

        bookRental.setRentalDate(LocalDate.now());


        model.addAttribute("addedBookRental", bookRental);


        return "rentBook";
    }

    @PostMapping(value = "/addBookRentalByTitle")
    public String addBookRentalForm(@Valid @ModelAttribute(name = "addedBookRental") BookRental addedBookRental, BindingResult bindingResult, Model model) {

        if (addedBookRental.getRentalDate() == null) {
            addedBookRental.setRentalDate(LocalDate.now());
        }


        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getFieldErrors());
            return addBookRentalForm(model);
        } else {
            addedBookRental.setDateToBeReturned(addedBookRental.getRentalDate());
            LocalDate dateToBeReturned = addedBookRental.getDateToBeReturned();
            model.addAttribute("dateToBeReturned", dateToBeReturned);

            bookRentalService.addBookRental(addedBookRental);


            return "bookRentalWithId";
        }

    }


    @GetMapping(value = "/returnBook", produces = MediaType.TEXT_HTML_VALUE)

    public String getBookReturnForm(Model model) {
        List<LibraryClient> clientList = libraryClientService.findClientsWithBooksToBeReturned();
        model.addAttribute("clientList", clientList);
        BookRental returnedBookRental = new BookRental();
        returnedBookRental.setActualReturnDate(LocalDate.now());
        model.addAttribute("returnedBookRental", returnedBookRental);


        return "returnBook";
    }


    @PostMapping(value = "/returnBook")
    public String getBookReturnForm(@Valid @ModelAttribute(name = "returnedBookRental") BookRental returnedBookRental, BindingResult bindingResult, Model model) throws BookRentalNotFoundException {
        if (bindingResult.hasErrors()) {

            System.out.println(bindingResult.getFieldErrors());
            List<LibraryClient> clientList = libraryClientService.findClientsWithBooksToBeReturned();

            returnedBookRental.setActualReturnDate(LocalDate.now());
            model.addAttribute("returnedBookRental", returnedBookRental);


            model.addAttribute("clientList", clientList);
            return getBookReturnForm( model);
        } else {

            Long returnedId = returnedBookRental.getId();//pobieramy id ze strony zwrotu
            BookRental byId = bookRentalService.findById(returnedId);//znajdujemy w bazie book rental o id wskazanym na stronie
            LocalDate actualReturnDate = returnedBookRental.getActualReturnDate();//pobieramy zapisane date zwrotu ze strony
            byId.setActualReturnDate(actualReturnDate);//zapisujemy do bazy danych nowa date zwrotu zgodnie z tym co na widoku


            bookRentalService.returnBook(byId);
            return "homeLibrary";
        }
    }

    @GetMapping(value = "/returnBook/{clientId}")
    public @ResponseBody
    List<String> getBooksRentedByClient(@PathVariable("clientId") Long clientId) {
        return bookRentalService.findBooksRentedByClient(clientId);
    }

    @GetMapping(value = "/returnBook/{clientId}/{bookTitle}")
    public @ResponseBody
    Long getBookRentalIdByClientIdAndBookTitle(@PathVariable("clientId") Long clientId, @PathVariable("bookTitle") String bookTitle) {
        return bookRentalService.findBookRentalByClientIdAndBookTitle(clientId, bookTitle);
    }


//







    /*@GetMapping(value = "/bookRental", produces = MediaType.TEXT_HTML_VALUE)
    public String findBookRentalById(@RequestParam(required=false,value="id") Long id, Model model) {
        try {
            BookRental bookRentalById = bookRentalService.findById(id);
            model.addAttribute("bookRentalById", bookRentalById);
            return "bookRentalWithId";
        } catch (BookRentalNotFoundException e) {

            return null;
        }
    }
*/

}
