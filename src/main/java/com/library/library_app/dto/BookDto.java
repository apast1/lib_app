package com.library.library_app.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.library.library_app.entity.Author;
import com.library.library_app.entity.Publisher;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class BookDto {
    private Long id;
    @NotEmpty(message = "Please enter title!")
    @Pattern(regexp = "^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ?!\\s]+$", message=" Title contains only letters, numbers, ? and !")
    private String title;

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    @NotNull(message = "Please enter author!")
    @Valid
    private Author author;
    //@NotNull
    //@Valid
    private Publisher publisher;
    private String bookType;
    private Boolean isBorrowed;

    public Boolean getBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(Boolean borrowed) {
        isBorrowed = borrowed;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public String getBookType() {
        return bookType;
    }

    public BookDto(Long id, String title, Author author, Publisher publisher, String bookType, Boolean isBorrowed) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.bookType = bookType;
        this.isBorrowed = isBorrowed;
    }

    public BookDto() {
    }
}
