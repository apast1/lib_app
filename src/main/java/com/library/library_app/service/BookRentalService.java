package com.library.library_app.service;

import com.library.library_app.entity.BookRental;
import com.library.library_app.exception.BookRentalNotFoundException;

import java.time.LocalDate;
import java.util.List;

public interface BookRentalService {
    public BookRental addBookRental(BookRental bookRental);
    public BookRental returnBook(BookRental bookRental);


    public BookRental findById(Long id) throws BookRentalNotFoundException;

    public List<String> findBooksRentedByClient(Long id);
    public Long findBookRentalByClientIdAndBookTitle(Long id, String title);
    public LocalDate findDateOfPossibleReturn(Long id);

}
