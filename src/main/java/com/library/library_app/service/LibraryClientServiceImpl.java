package com.library.library_app.service;

import com.library.library_app.entity.Book;
import com.library.library_app.entity.LibraryClient;
import com.library.library_app.exception.BookNotFoundException;
import com.library.library_app.exception.ClientNotFoundException;
import com.library.library_app.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LibraryClientServiceImpl implements LibraryClientService {

    private ClientRepository clientRepository;

    @Autowired
    public LibraryClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public List<LibraryClient> findAll() {
        Iterable<LibraryClient> clientRepositoryAll = clientRepository.findAll();

        List<LibraryClient> libraryClientList = new ArrayList<>();

        for (LibraryClient client : clientRepositoryAll) {
            libraryClientList.add(client);
        }
        return libraryClientList;
    }

    @Override
    public LibraryClient findByCardId(Long id) throws ClientNotFoundException {


        Optional<LibraryClient> clientByIdOptional = clientRepository.findById(id);
        if (clientByIdOptional.isPresent()) {

            LibraryClient libraryClient = clientByIdOptional.get();
            return libraryClient;
        } else
            throw new ClientNotFoundException("Client of id " + id + "  does not exist in DB");


    }

    @Override
    public List<LibraryClient> findClientsWithBooksToBeReturned() {
        return clientRepository.findClientsWithBooksToBeReturned();
    }
}
