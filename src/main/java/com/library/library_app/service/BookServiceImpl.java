package com.library.library_app.service;

import com.library.library_app.converter.BookConverter;
import com.library.library_app.converter.BookDtoConverter;
import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Author;
import com.library.library_app.entity.Book;
import com.library.library_app.exception.BookNotFoundException;
import com.library.library_app.repository.AuthorRepository;
import com.library.library_app.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;
    private BookDtoConverter bookDtoConverter;
    private BookConverter bookConverter;


    @Autowired
    public BookServiceImpl(BookRepository bookRepository, BookDtoConverter bookDtoConverter, BookConverter bookConverter) {
        this.bookRepository = bookRepository;
        this.bookDtoConverter = bookDtoConverter;
        this.bookConverter = bookConverter;

    }

    @Override
    public List<BookDto> findAll() {
        Iterable<Book> allBooks = bookRepository.findAll();

        List<BookDto> bookList = new ArrayList<>();

        for (Book book : allBooks) {
            BookDto booDto = bookDtoConverter.apply(book);
            bookList.add(booDto);
        }


        return bookList;
    }

    @Override
    public BookDto findById(Long id) throws BookNotFoundException {
        Optional<Book> bookByIdOptional = bookRepository.findById(id);
        if (bookByIdOptional.isPresent()) {

            Book book = bookByIdOptional.get();
            return bookDtoConverter.apply(book);
        } else
            throw new BookNotFoundException("Book of id " + id + "  does not exist in DB");


    }

    @Override
    public BookDto addBookDto(BookDto bookDto) {

        Book book = bookConverter.apply(bookDto);

        bookRepository.save(book);

        return bookDtoConverter.apply(book);
    }

    @Override
    public List<String> findAllTitles() {
        List<BookDto> allBooks = findAll();
        List<String> allTitles = new ArrayList<>();
        for (BookDto bookDto : allBooks) {
            allTitles.add(bookDto.getTitle());
        }
        return allTitles;
    }

    @Override
    public List<BookDto> findBooksNotRented() {

        List<BookDto> allBooksInLibrary = findAll();


       List<BookDto> notRentedBooks= allBooksInLibrary.
                stream()
                .filter(b -> b.getBorrowed().equals(Boolean.FALSE))
                .collect(Collectors.toList());


        return notRentedBooks;
    }

    @Override
    public List<String> findBooksRentedByClient(Long id) {
        List<String> booksRentedByClient =findBooksRentedByClient(id);

        return booksRentedByClient;
    }
}
