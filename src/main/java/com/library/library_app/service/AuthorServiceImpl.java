package com.library.library_app.service;

import com.library.library_app.converter.BookConverter;
import com.library.library_app.converter.BookDtoConverter;
import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Author;
import com.library.library_app.entity.Book;
import com.library.library_app.exception.AuthorNotFoundException;
import com.library.library_app.exception.BookNotFoundException;
import com.library.library_app.repository.AuthorRepository;
import com.library.library_app.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService {

    private AuthorRepository authorRepository;


    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;

    }

    @Override
    public List<Author> findAll() {
        Iterable<Author> allAuthors = authorRepository.findAll();

        List<Author> authorList = new ArrayList<>();

        for (Author author : allAuthors) {

            authorList.add(author);
        }


        return authorList;
    }

    @Override
    public Author findAuthor(String firstName, String lastName) {
        Optional<Author> authorByNameOptional = authorRepository.findByFirstNameAndLastName(firstName, lastName);
        if (authorByNameOptional.isPresent()) {

            return authorByNameOptional.get();
        } else {
            Author newAuthor = new Author();
            newAuthor.setFirstName(firstName);
            newAuthor.setLastName(lastName);
            return newAuthor;
        }




    }
//testuje custom query
    @Override
    public List<Author> findAuthorByFirstId() {

        List<Author> authorByFirstName = authorRepository.findAuthorByFirstId( );
        return authorByFirstName;
    }

    @Override
    public List<Author> findAuthorById() {


        List<Author> authorById = authorRepository.findAuthorById();
        return authorById;
    }
}

