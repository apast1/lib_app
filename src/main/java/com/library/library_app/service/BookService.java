package com.library.library_app.service;

import com.library.library_app.dto.BookDto;
import com.library.library_app.exception.BookNotFoundException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface BookService {
    public List<BookDto> findAll();

    public BookDto findById(Long id) throws BookNotFoundException;

    public BookDto addBookDto(BookDto bookDto);

    public List<String> findAllTitles();

    public List<BookDto> findBooksNotRented();

    @Query(value = "SELECT title  FROM book b, book_rental r WHERE b.id=r.book_id AND r.library_client_id=:id",nativeQuery = true)
    public List<String> findBooksRentedByClient(@Param("id") Long id);


}
