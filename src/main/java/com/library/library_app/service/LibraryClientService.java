package com.library.library_app.service;

import com.library.library_app.entity.LibraryClient;
import com.library.library_app.exception.ClientNotFoundException;

import java.util.List;

public interface LibraryClientService {

    public List<LibraryClient> findAll();
    public LibraryClient findByCardId(Long id) throws ClientNotFoundException;
    public List<LibraryClient>findClientsWithBooksToBeReturned();

}
