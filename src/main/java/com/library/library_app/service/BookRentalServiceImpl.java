package com.library.library_app.service;

import com.library.library_app.entity.BookRental;
import com.library.library_app.exception.BookRentalNotFoundException;
import com.library.library_app.repository.BookRentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class BookRentalServiceImpl implements BookRentalService {

    private BookRentalRepository bookRentalRepository;

    @Autowired
    public BookRentalServiceImpl(BookRentalRepository bookRentalRepository) {
        this.bookRentalRepository = bookRentalRepository;
    }

    @Override
    public BookRental addBookRental(BookRental bookRental) {
        bookRental.getBook().setBorrowed(Boolean.TRUE);


        BookRental savedBookRental = bookRentalRepository.save(bookRental);

        return savedBookRental;
    }

    @Override
    public BookRental returnBook(BookRental bookRental) {
        Long returnedBookRentalId = bookRental.getId();


           bookRental.getBook().setBorrowed(Boolean.FALSE);
            BookRental changedBookRental = bookRentalRepository.save(bookRental);


        return changedBookRental;
    }

    @Override
    public BookRental findById(Long id) throws BookRentalNotFoundException {
        Optional<BookRental> bookRentalById = bookRentalRepository.findById(id);

        if (bookRentalById.isPresent()) {
            return bookRentalById.get();
        } else {
            throw new BookRentalNotFoundException("There is no such Bookrental of id:  " + id);
        }


    }

    @Override
    public List<String> findBooksRentedByClient(Long id) {
        List<String> booksRentedByClient = bookRentalRepository.findBooksRentedByClient(id);
        return booksRentedByClient;
    }

    @Override
    public Long findBookRentalByClientIdAndBookTitle(Long id, String title) {
        Long bookRentalByClientIdAndBookTitle = bookRentalRepository.findBookRentalByClientIdAndBookTitle(id, title);
        System.out.println(bookRentalByClientIdAndBookTitle);
        return bookRentalByClientIdAndBookTitle;
    }

    @Override
    public LocalDate findDateOfPossibleReturn(Long id) {
        LocalDate dateOfPossibleReturn = bookRentalRepository.findDateOfPossibleReturn(id);
        return dateOfPossibleReturn;
    }

}
