package com.library.library_app.service;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Author;
import com.library.library_app.exception.AuthorNotFoundException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AuthorService {
    public List<Author> findAll();

    public Author findAuthor(String firstName, String lastName);




    public List<Author> findAuthorByFirstId();
    public List<Author>findAuthorById();

}
