package com.library.library_app.service;

import com.library.library_app.entity.Publisher;
import com.library.library_app.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class PublisherServiceImpl implements PublisherService {

    private PublisherRepository publisherRepository;

    @Autowired
    public PublisherServiceImpl(PublisherRepository publisherRepository){
        this.publisherRepository=publisherRepository;
    }
    @Override
    public List<Publisher> findAll() {
        Iterable<Publisher> all = publisherRepository.findAll();
        List<Publisher>publisherList=new ArrayList<>();

        for (Publisher publisher:all) {
            publisherList.add(publisher);
        }

        return publisherList;
    }
}
