package com.library.library_app.converter;

import com.library.library_app.dto.BookDto;
import com.library.library_app.entity.Book;
import com.library.library_app.entity.BookType;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BookConverter implements Function<BookDto, Book> {
    @Override
    public Book apply(BookDto bookDto) {
        Book book = new Book();
        book.setId(bookDto.getId());
        book.setTitle(bookDto.getTitle());
        book.setAuthor(bookDto.getAuthor());
        book.setPublisher(bookDto.getPublisher());

        String bookDtoType = bookDto.getBookType();
        if (bookDtoType == null) {
            bookDtoType = "DRAMA";
        }

        BookType bookType = BookType.valueOf(bookDtoType);
        book.setBookType(bookType);
        book.setPages(0);
        book.setBorrowed(false);
        book.setBookRental(null);
        return book;
    }
}

