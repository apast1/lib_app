package com.library.library_app.converter;

        import com.library.library_app.dto.BookDto;
        import com.library.library_app.entity.Book;
        import com.library.library_app.entity.BookType;
        import org.springframework.stereotype.Component;

        import java.util.function.Function;

@Component
public class BookDtoConverter implements Function<Book, BookDto> {


    @Override
    public BookDto apply(Book book) {

        BookType bookType=book.getBookType();
       String bookDtoType= bookType.name();



        return new BookDto(book.getId(),book.getTitle(),book.getAuthor(),book.getPublisher(),bookDtoType,book.getBorrowed());
    }
}
