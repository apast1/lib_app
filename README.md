# Home Library - app for storing, adding and renting books




Home Library is SpringBoot app designed for the learning purposes of my own. It has all the features modern library should have:

  - You may check all books in the library and see if they are available for rent. If not - you may check when they're going to be returned.
  - You may add books to the library - by filling in a form
  - You may rent books
  - But also REMEMBER to return them :-)

# Are you curious how does it look like? 

  ![](./allBooks.JPG)
  
  ![](./addingBooks.JPG)
  
  ![](./rentingBooks.JPG)
  
  ![](./returnBook.JPG)
  




### Tech



* [JAVA 8](https://www.java.com/pl/download/) 
* [Spring](https://spring.io/)
* [MySQL](https://www.mysql.com/)
* [JUnit4](https://junit.org/junit4/)
* [Thymeleaf](https://www.thymeleaf.org/)




### Todos

 - Write MORE Tests
 - Add searching by Title, BookType, Author

